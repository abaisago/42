/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_llitoa_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@tuta.io>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/14 14:49:11 by abaisago          #+#    #+#             */
/*   Updated: 2021/01/07 19:37:11 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

#include <limits.h>
#include <stdlib.h>

/*
** Returns an allocated representation string of a
** 64-bit signed integer in a specified base of up to 36.
*/

char			*ft_llitoa_base(long long value, uint8_t base)
{
	size_t		at;
	char		*res;

	if (base > 36)
		return (NULL);
	if (value == LLONG_MIN && base == 10)
		return (ft_strdup("-9223372036854775808"));
	if ((res = (char*)ft_strnew(1 + ft_count_digits(value, base))) == NULL)
		return (NULL);
	at = 0;
	if (ft_llabs_is(&value) && base == 10)
	{
		res[0] = '-';
		at = 1;
	}
	ft_getnbr_base(value, base, res, &at);
	res[at] = '\0';
	return (res);
}
