/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@tuta.io>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 10:30:50 by abaisago          #+#    #+#             */
/*   Updated: 2021/01/07 19:51:22 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	void	cycle(long long nbr, int fd)
{
	if (nbr >= 10)
	{
		cycle(nbr / 10, fd);
		cycle(nbr % 10, fd);
	}
	else
		ft_putchar_fd('0' + nbr, fd);
}

void			ft_putnbr_fd(int n, int fd)
{
	long long	nbr;

	nbr = n;
	if (ft_llabs_is(&nbr))
		ft_putchar_fd('-', fd);
	cycle(nbr, fd);
}
